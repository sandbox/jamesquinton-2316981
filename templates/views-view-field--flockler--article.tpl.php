<?php $article = json_decode($row->{$field->field_alias}); ?>
<div class="flockler-items items clearfix">

	<?php if($article->display_style === 'image') : /**** Image ****/ ?>
	  <div class="item <?php print $article->display_style; ?>" id="<?php print $article->id; ?>">
	    <a href="<?php print $article->url; ?>" target="_blank">
	      <img class="nl" data-src="<?php print $article->cover_url; ?>" src="/<?php print drupal_get_path('module', 'flockler'); ?>/img/blank.gif" alt="<?php print $article->title; ?>" title="<?php print $article->title; ?>" />
	    </a>
	  </div>
	<?php elseif ($article->display_style === 'video') : /**** Video ****/ ?>
		<div class="item <?php print $article->display_style; ?>" id="<?php print $article->id; ?>">
			<div class="video-container">
				<iframe src="<?php print $article->video->embed_src; ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
	  </div>
	<?php elseif ($article->display_style === 'article') : /**** Article ****/ ?>
		<div class="item <?php print $article->display_style; ?>" id="<?php print $article->id; ?>">
	    <?php print $article->body; ?>
	  </div>
  <?php elseif ($article->display_style === 'tweet') : /**** Tweet ****/ ?>
    <div class="item <?php print $article->display_style; ?>" id="<?php print $article->id; ?>">
      <div class="author-avatar">
        <img src="<?php print $article->tweet->profile_image_url; ?>" alt="<?php print $article->tweet->name; ?>'s avatar" />
      </div>
      <div class="tweet-author">
        <p><?php print $article->tweet->name; ?></p>
      </div>
      <div class="tweet-body">
        <p><?php print $article->tweet->text; ?></p>
      </div>
      <div class="tweet-source">
        <a href="<?php print $article->full_url ?>"><?php print $article->title ?></a>
      </div>
    </div>
	<?php else: /**** Other article types ****/ ?>
		<?php print_r($article); ?>
	<?php endif; ?>

</div>

