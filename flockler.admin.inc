<?php

/**
 * Implements hook_admin()
 * @return array Admin form
 */
function flockler_admin() {
	$form = array();

	$form['flockler_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Flockler site ID'),
    '#default_value' => variable_get('flockler_site_id', ''),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t("Enter your Flockler site ID.")
  );

  return system_settings_form($form);
}
