(function($) {

  Drupal.behaviors.flocklerBehavior = {
    attach: function (context, settings) {

      var self = this;

      function init() {
        self.bindEvents();
        self.lazyLoadImages();
      }

      self.bindEvents = function () {
        $('a.more-flockler', context).click(function (e) {
          e.preventDefault();
          var lastId = $('.flockler-items .item:last').attr('id');
          self.moreItems(lastId, 5);
        });
      }

      self.moreItems = function (start, end) {
        $.get('/flockler-api-items/' + start + '/' + end, function (data) {
          $('.flockler-items').append($(data).find('.item'));
          if ($(data)[2] == undefined) {
            $('.more-flockler').remove();
          }
          self.lazyLoadImages();
        }, 'html');
      }

      self.lazyLoadImages = function () {
        $.each($('.flockler-items img.nl'), function (i,e) {
          var src = $(this).attr('data-src');
          $(this).attr('src', src);
          $(this).removeClass('nl');
        });
      }

      init();
    }
  }
})(jQuery);
